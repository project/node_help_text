Help module for Drupal Content types

This module Provides the ability to enter some help text for end users,
admin can create/enter the help text,
then after configuring the block users
can access that block in the front end.

Installation

- Install and enable node_help_text module.
- Go to the "admin/content/help" and add the help text for each content type.
- go to the blocks, there you will see a new "Help" block.
- Configure on which pages you want to show the help block.
