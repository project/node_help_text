<?php

/**
 * @file
 * Node help text.
 */

/**
 * Implements hook_menu().
 */
function node_help_text_menu() {
  $items['admin/content/help'] = array(
    'title' => 'Content type help',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('node_help_text_form'),
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => array('administer site configuration') ,
  );
  return $items;
}

/**
 * Form Configurations().
 */
function node_help_text_form($form, &$form_state) {
  $node_types = node_type_get_types();
  $value_field = variable_get('node_help_text_field_values');
  foreach ($node_types as $node_types_data) {
    $nname = $node_types_data->name;
    $ntype = $node_types_data->type;
    $form[$ntype . '_nodetype'] = array(
      '#title' => t("Help text for: @type", array('@type' => $nname)),
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#description' => t("Add help text for: @type", array('@type' => $nname)),
      '#default_value' => isset($value_field[$ntype]) ? $value_field[$ntype] : '',
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Form submission handler for node_help_text_form().
 */
function node_help_text_form_submit($form, &$form_state) {
  $node_types = node_type_get_types();
  $value_field = array();
  foreach ($node_types as $node_types_data) {
    $ntype = $node_types_data->type;
    $value_field[$ntype] = $form_state['values'][$ntype . '_nodetype']['value'];
  }
  variable_set('node_help_text_field_values', $value_field);
  drupal_set_message(t('Saved'));
}

/**
 * Implements hook_block_info().
 */
function node_help_text_block_info() {
  $blocks['help-text'] = array(
    'info' => t('Content Type Help'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function node_help_text_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'help-text':
      $block['subject'] = t('Content type help');
      $block['content'] = node_help_text_view_content();
      break;
  }
  return $block;
}

/**
 * Getting the block content here.
 */
function node_help_text_view_content() {
  $node = menu_get_object();
  if (is_object($node) && isset($node->type)) {
    $ntype = $node->type;
    $value_field = variable_get('node_help_text_field_values');
    $output = $value_field[$ntype];
    return $output;
  }
}

/**
 * Implements hook_help().
 */
function node_help_text_help($path, $arg) {
  switch ($path) {
    // Main module help for the node_help_text module.
    case 'admin/help#node_help_text':
      return '<p>' . t('Install and enable node_help_text module.<br/>Go to the "admin/content/help" and add the help text for each content type.<br/>go to the blocks, there you will see a new "Help" block.<br/>Configure on which pages you want to show the help block.') . '</p>';

    // Help for another path in the node_help_text module.
    case 'admin/content/help':
      return '<p>' . t('Here all the content type help text can be saved.') . '</p>';
  }
}
